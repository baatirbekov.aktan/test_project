
@extends('layouts.app')

@section('content')

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">{{ __('Product name') }}</th>
            <th scope="col">{{ __('From') }}</th>
            <th scope="col">{{ __('To') }}</th>
            <th scope="col">{{ __('Author') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($searches as $search)
            <tr>
                <th scope="row">{{ $search->id }}</th>
                <td>{{ $search->name }}</td>
                <td>{{ $search->from }}</td>
                <td>{{ $search->to }}</td>
                <td>{{ $search->user->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $searches->links() }}

@endsection
