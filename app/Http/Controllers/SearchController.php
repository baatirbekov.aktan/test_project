<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Search;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        if (Auth::user()->hasRole(Role::BUYER)) {
            $searches = Auth::user()->searches()->with('user')->paginate(10);
        } else {
            $searches = Search::query()->matchSearches()->with('user')->paginate(10);
        }

        return view('searches.index', compact('searches'));
    }
}
