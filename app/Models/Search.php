<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Search extends Model
{
    use HasFactory;

    protected $fillable = ['type_id', 'user_id', 'name', 'from', 'to'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * @param Builder $query
     * @return void
     */
    public function scopeMatchSearches(Builder $query): void
    {
        $user = Auth::user();
        $from = $user->offers()->min('from');
        $to = $user->offers()->max('to');
        $names = $user->offers()->pluck('name');
        $typesIds = $user->offers()->pluck('type_id')->unique('type_id');

        $query->where('user_id', '!=', $user->id)
            ->whereIn('name', $names)
            ->whereIn('type_id', $typesIds)
            ->where('from', '>=', $from)
            ->where('to', '<=', $to)
            ->with('user');
    }
}
