<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;

    use HasFactory;

    public const BUYER = 'buyer';

    public const SALESMAN = 'salesman';

    public CONST ROLES = [self::BUYER, self::SALESMAN];

    protected $fillable = ['name'];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'users_roles');
    }
}
