stop:
	docker compose stop
shell:
	docker compose exec app sh
start:
	docker compose up --detach
pull:
	docker compose pull
destroy:
	docker compose down --remove-orphans --volumes
build:
	docker compose build --pull
seed:
	docker compose exec app php artisan db:seed
migrate:
	docker compose exec app php artisan migrate
migrate-fresh:
	docker compose exec app php artisan migrate:fresh
npm-install:
	docker compose exec app npm install
npm-build:
	docker compose exec app npm run build
composer-install:
	docker compose exec app composer install
copy-env:
	cp .env.example .env
key-generate:
	docker compose exec app php artisan key:generate
init: copy-env destroy build start composer-install key-generate migrate seed npm-install npm-build

