<?php

namespace Database\Seeders;

use App\Models\Offer;
use App\Models\Role;
use App\Models\Search;
use App\Models\Type;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buyerRole = Role::query()->where('name', '=', 'buyer')->first();
        $salesmanRole = Role::query()->where('name', '=', 'salesman')->first();

        $types = Type::all();

        foreach ($types as $type) {
            $buyers = User::factory(2)->has(
                Search::factory()->for($type)->count(100)
            )->create();

            $buyerRole->users()->attach($buyers->pluck('id'));

            $salesmen = User::factory(2)->has(
                Offer::factory()->for($type)->count(5)
            )->create();

            $salesmanRole->users()->attach($salesmen->pluck('id'));
        }
    }
}
